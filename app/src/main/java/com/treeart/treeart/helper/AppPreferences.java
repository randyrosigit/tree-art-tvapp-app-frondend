package com.treeart.treeart.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private static final String PREF_NAME = "Simpool";
    private static final int PREF_MODE = 0;
    private static final String POSITION_VIDEO = "POSITION_VIDEO";
    private static final String USERNAME = "USERNAME";
    private static final String ACTIVE_FINGERPRINT = "ACTIVEFINGERPRINT";
    private static final String PASSWORD = "PASSWORD";
    private static final String ISFIRST = "ISFIRST";
    private static final String SELECTED_ACCOUNT = "SELECTEDACCOUNT";
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    public AppPreferences(Context context) {
        mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(PREF_NAME, PREF_MODE);
    }



//    public LoginResponse getUser() {
//        Gson gson = new Gson();
//        Type type = new TypeToken<LoginResponse>() {
//        }.getType();
//        String json = mSharedPreferences.getString(USER_SESSION, "{}");
//        LoginResponse obj = gson.fromJson(json, type);
//        if (obj != null) {
//            return obj;
//        } else return null;
//    }
//
//    public void setUser(LoginResponse user) {
//        Gson gson = new Gson();
//        String json = gson.toJson(user);
//        mEditor = mSharedPreferences.edit();
//        mEditor.putString(USER_SESSION, json);
//        mEditor.apply();
//    }
//
    public Long getPositionVideo(){
        return mSharedPreferences.getLong(POSITION_VIDEO, 0);
    }
//
//    public String getPassword(){
//        return mSharedPreferences.getString(PASSWORD, "");
//    }
//
//    public int getSelectedAccount(){
//        return mSharedPreferences.getInt(SELECTED_ACCOUNT, 0);
//    }
//
//    public void setSelectedAccount(int position){
//        mEditor = mSharedPreferences.edit();
//        mEditor.putInt(SELECTED_ACCOUNT, position);
//        mEditor.apply();
//    }
//
    public void setPositionVideo(Long positionVideo){
        mEditor = mSharedPreferences.edit();
        mEditor.putLong(POSITION_VIDEO, positionVideo);
        mEditor.apply();
    }
//
//    public void setPassword(String password){
//        mEditor = mSharedPreferences.edit();
//        mEditor.putString(PASSWORD, password);
//        mEditor.apply();
//    }
//
//    public void setActiveFingerprint(Boolean status){
//        mEditor = mSharedPreferences.edit();
//        mEditor.putBoolean(ACTIVE_FINGERPRINT, status);
//        mEditor.apply();
//    }
//
//    public Boolean isActiveFingerprint(){
//        return mSharedPreferences.getBoolean(ACTIVE_FINGERPRINT, false);
//    }
//
//    public void setFirst(Boolean status){
//        mEditor = mSharedPreferences.edit();
//        mEditor.putBoolean(ISFIRST, status);
//        mEditor.apply();
//    }
//
//    public Boolean isFirst(){
//        return mSharedPreferences.getBoolean(ISFIRST, true);
//    }


}