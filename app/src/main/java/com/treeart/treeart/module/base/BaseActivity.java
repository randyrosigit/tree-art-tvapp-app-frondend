package com.treeart.treeart.module.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.treeart.treeart.helper.AppPreferences;

/**
 * @Author SahabatDeveloper
 * @CreatedAt 18 July 2018
 * @Company Ikkat.io
 */
public class BaseActivity extends Activity {

    ProgressDialog pDialog;
    public AppPreferences appPreferences;
    public DecimalFormat df= new DecimalFormat("#,##0");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ITALY));
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Terhubung ke Server");
        pDialog.setCanceledOnTouchOutside(false);
        appPreferences = new AppPreferences(this);
    }

    public void launchActivity(Class<?> kelas){
        startActivity(new Intent(this,kelas));
    }

    public void launchActivity(Class<?> kelas, Bundle bundle){
        Intent i = new Intent(this,kelas);
        i.putExtras(bundle);
        startActivity(i);
    }

    public void showDialog(){
        pDialog.show();
    }

    public void hideDialog(){
        pDialog.hide();
    }

//    protected void replaceFragment(@IdRes int containerViewId,
//                                   @NonNull Fragment fragment,
//                                   @NonNull String fragmentTag,
//                                   @Nullable String backStackStateName) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(containerViewId, fragment, fragmentTag)
//                .addToBackStack(backStackStateName)
//                .commit();
//    }
}