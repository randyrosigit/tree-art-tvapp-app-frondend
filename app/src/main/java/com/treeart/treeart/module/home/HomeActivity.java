package com.treeart.treeart.module.home;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.treeart.treeart.R;
import com.treeart.treeart.module.base.BaseActivity;

public class HomeActivity extends BaseActivity {
    RecyclerView rvAlbum;

    List<String> listAlbum  = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        rvAlbum = findViewById(R.id.rv_album);

        listAlbum.add("Album 1");
        listAlbum.add("Album 2");
        listAlbum.add("Album 3");
        listAlbum.add("Album 4");
        listAlbum.add("Album 5");
        listAlbum.add("Premium Member Only");

        rvAlbum.setHasFixedSize(true);
        rvAlbum.setLayoutManager(new GridLayoutManager(this,3));
        rvAlbum.setAdapter(new AlbumAdapter(listAlbum,this));
    }
}
