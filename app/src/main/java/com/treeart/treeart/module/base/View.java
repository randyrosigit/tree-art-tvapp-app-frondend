package com.treeart.treeart.module.base;

public interface View {
    public void onShowLoading();
    public void onHideLoading();
    public void onFailureMessage(String message);
    public void onFailureMessage(int resId);
}