package com.treeart.treeart.module.album;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.treeart.treeart.R;
import com.treeart.treeart.module.video.VideoActivity;

public class IsiAlbumAdapter extends RecyclerView.Adapter<IsiAlbumAdapter.ViewHolder> {
    Context context;

    public IsiAlbumAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public IsiAlbumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_isi_album,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull IsiAlbumAdapter.ViewHolder holder, int i) {
        holder.linearEpisode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,VideoActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearEpisode;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            linearEpisode = itemView.findViewById(R.id.linear_episode);
        }
    }
}
