package com.treeart.treeart.module.album;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.treeart.treeart.R;
import com.treeart.treeart.module.base.BaseActivity;

public class AlbumActivity extends BaseActivity {

    RecyclerView rvIsiAlbum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        rvIsiAlbum = findViewById(R.id.rv_isi_album);
        rvIsiAlbum.setHasFixedSize(true);
        rvIsiAlbum.setLayoutManager(new LinearLayoutManager(this));
        rvIsiAlbum.setAdapter(new IsiAlbumAdapter(this));
    }
}
