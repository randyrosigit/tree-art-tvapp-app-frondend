package com.treeart.treeart.module.home;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import com.treeart.treeart.R;
import com.treeart.treeart.module.album.AlbumActivity;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
    List<String> listAlbum;
    Context context;

    public AlbumAdapter(List<String> listAlbum, Context context) {
        this.listAlbum = listAlbum;
        this.context = context;
    }

    @NonNull
    @Override
    public AlbumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_album,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.ViewHolder holder, int i) {
        holder.tvAlbum.setText(listAlbum.get(i));
        holder.relativeAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,AlbumActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listAlbum.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAlbum;
        RelativeLayout relativeAlbum;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvAlbum = itemView.findViewById(R.id.tv_album);
            relativeAlbum = itemView.findViewById(R.id.relative_album);
        }
    }
}
