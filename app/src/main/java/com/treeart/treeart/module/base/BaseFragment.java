package com.treeart.treeart.module.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.treeart.treeart.helper.AppPreferences;

/**
 * @Author SahabatDeveloper
 * @CreatedAt 19 July 2018
 * @Company Ikkat.io
 */
public class BaseFragment extends Fragment {

    ProgressDialog pDialog;
    public AppPreferences appPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPreferences = new AppPreferences(getActivity());
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Terhubung ke Server");
        pDialog.setCanceledOnTouchOutside(false);
    }

    public void launchActivity(Class kelas){
        startActivity(new Intent(getActivity(),kelas));
    }

    public void launchActivity(Class<?> kelas, Bundle bundle){
        Intent i = new Intent(getActivity(),kelas);
        i.putExtras(bundle);
        startActivity(i);
    }

    public void showDialog(){
        pDialog.show();
    }

    public void hideDialog(){
        pDialog.hide();
    }
}