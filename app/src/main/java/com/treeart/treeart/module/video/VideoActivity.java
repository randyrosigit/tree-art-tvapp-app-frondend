package com.treeart.treeart.module.video;

import android.net.Uri;
import android.os.Bundle;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import com.treeart.treeart.R;
import com.treeart.treeart.module.base.BaseActivity;

public class VideoActivity extends BaseActivity {

    PlayerView playerView;
    SimpleExoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        playerView = findViewById(R.id.player_view);

    }

    @Override
    protected void onStart() {
        super.onStart();

        player = ExoPlayerFactory.newSimpleInstance(this,new DefaultTrackSelector());
        playerView.setPlayer(player);

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this,Util.getUserAgent(this,"TreeArt"));

        ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("http://acityanusantara.com/wp-content/minion.mp4"));

        ConcatenatingMediaSource mediaSource1 = new ConcatenatingMediaSource();
        mediaSource1.addMediaSource(mediaSource);
        mediaSource1.addMediaSource(mediaSource);
        mediaSource1.addMediaSource(mediaSource);

        player.prepare(mediaSource1);
        player.seekTo(appPreferences.getPositionVideo());
        player.setPlayWhenReady(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        appPreferences.setPositionVideo(player.getCurrentPosition());
        playerView.setPlayer(null);
    }

    @Override
    public void onBackPressed() {
        appPreferences.setPositionVideo(player.getCurrentPosition());
        playerView.setPlayer(null);
        super.onBackPressed();
    }
}
